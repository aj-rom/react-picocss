export default function getClassName(className, propValue) {
  return { className: `${ propValue || '' } ${ className }`.trim() }
}
