import React from 'react'
import PropTypes from 'prop-types'
import getClassName from './utils/util'

function Button(props) {

    let classToAdd = ''
    if(props.outline) {
        classToAdd = 'outline'
    }

    const classes = getClassName(classToAdd, props.className)

    return (
        <button { ...props } { ...classes }>
            { props.children }
        </button>
    )
}

function Secondary(props) {
    const classes = getClassName('secondary', props.className)
    return (
        <Button { ...props } { ...classes }>
            { props.children }
        </Button>
    )
}

function Contrast(props) {
    const classes = getClassName('contrast', props.className)
    return (
        <Button { ...props } { ...classes }>
            { props.children }
        </Button>
    )
}

Button.propTypes = {
    className: PropTypes.string,
    outline: PropTypes.bool
}

Button.defaultProps = {
    className: '',
}

// Secondary
Secondary.propTypes = Button.propTypes
Secondary.defaultProps = Button.defaultProps
Button.Secondary = Secondary

// Contrast
Contrast.propTypes = Button.propTypes
Contrast.defaultProps = Button.defaultProps
Button.Contrast = Contrast

export default Button
