import React from 'react'
import Card from './Card'
import PropTypes from 'prop-types'

function Modal(props) {
    return (
        <dialog { ...props }>
            <Card>
                { props.children }
            </Card>
        </dialog>
    )
}

Modal.propTypes = {
    children: PropTypes.node
}

export default Modal
