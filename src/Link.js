import React from 'react'
import PropTypes from 'prop-types'
import getClassName from './utils/util'

function Secondary(props) {
  const classes = getClassName('secondary', props.className)
  return (
    <Link { ...props } { ...classes }>
      { props.children }
    </Link>
  )
}

function Contrast(props) {
  const classes = getClassName('contrast', props.className)
  return (
    <Link { ...props } { ...classes }>
      { props.children }
    </Link>
  )
}

function Button(props) {
  return (
      <Link { ...props } role='button'>
        { props.children }
      </Link>
  )
}

function Link(props) {
  return (
    <a { ...props }>
      { props.children }
    </a>
  )
}

Link.propTypes = {
  className: PropTypes.string,
}

Link.defaultProps = {
  className: '',
}

// Secondary Button
Secondary.propTypes = Link.propTypes
Secondary.defaultProps = Link.defaultProps
Link.Secondary = Secondary

// Contrast Button
Contrast.propTypes = Link.propTypes
Contrast.defaultProps = Link.defaultProps
Link.Contrast = Contrast

// Link Button
Button.propTypes = Link.propTypes
Button.defaultProps = Link.defaultProps
Link.Button = Button

export default Link
