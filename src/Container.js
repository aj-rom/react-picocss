import React from 'react'
import PropTypes from 'prop-types'
import getClassName from './utils/util'

function Container(props) {
  const main = props.fluid ? 'container-fluid' : 'container'
  const classes = getClassName(main, props.className)
  return (
    <div { ...props } { ...classes }>
      { props.children }
    </div>
  )
}

Container.propTypes = {
  className: PropTypes.string,
  fluid: PropTypes.bool,
  children: PropTypes.node
}

export default Container
