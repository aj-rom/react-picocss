import React from 'react'
import PropTypes from 'prop-types'
import getClassName from './utils/util'

function Grid(props) {
  const classes = getClassName('grid', props.className)
  return (
    <div { ...props } { ...classes }>
      { props.children }
    </div>
  )
};

Grid.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
}

Grid.defaultProps = {
  className: '',
  children: null
}

export default Grid
