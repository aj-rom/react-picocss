import React from 'react'
import PropTypes from 'prop-types'

function Scroller(props) {
    return (
        <figure { ...props }>
            { props.children }
        </figure>
    )
}

Scroller.propTypes = {
    children: PropTypes.node
}

export default Scroller
