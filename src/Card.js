import React from 'react'
import PropTypes from 'prop-types'
import getClassName from './utils/util.js'

function Card(props) {
  const classes = getClassName('card', props.className)

  return (
    <div { ...props } { ...classes }>
      { props.children }
    </div>
  )
}

Card.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
}

Card.defaultProps = {
  className: '',
}

export default Card
