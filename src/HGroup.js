import React from 'react'
import PropTypes from 'prop-types'
import getClassName from './utils/util.js'

function HGroup(props) {
  const classes = getClassName('headings', props.className)

  return (
    <div { ...props } { ...classes }>
      { props.children }
    </div>
  )
}

const childLength = function (props, propName) {
  if (!Array.isArray(props.children) || props.children.length !== 2) {
    return new Error(`${ propName } needs to consist of 2 child elements.`)
  }

  return null
}

HGroup.propTypes = {
  className: PropTypes.string,
  children: childLength,
}

HGroup.defaultProps = {
  className: '',
}

export default HGroup
