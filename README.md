# react-picocss 🚀 
A simple react implementation of the [PicoCSS](https://picocss.com/) framework.

### What is this?
* **Mainly**. Component wrappers in React for the PicoCSS framework.
* **Minimal**. Using PicoCSS's minimal framework you will never need to write CSS again.
* **Expandable**. Add your own classes on-top of the Pico given `classNames`.

### What this isn't.
* **CSS Framework**. You must have PicoCSS installed in your react application.
* **CSS Compiler**. This only compiles javascript, specifically react elements, using babel. *No CSS is provided or compiled.*
This is up to you to compile your assets before runtime.

## Getting Started

### Installation
To install `react-picocss`, run:
```console
npm i react-picocss
```

Next there are a couple of peer dependencies. If you have React and PicoCSS installed you can skip to the [First Component](#first-component)

### Peer Dependencies
In order for this package to work, you must have [react](npmjs.com/package/react), [react-dom](https://www.npmjs.com/package/react-dom), and [@picocss/pico](https://www.npmjs.com/package/@picocss/pico) installed to your application.

#### PicoCSS Install
Installation instructions for PicoCSS.

##### Local (Recommended)

To install PicoCSS locally, run:
```console
npm i @picocss/pico
```
Next you need to add their CSS or SCSS to the main application pipeline.

You can do this by adding any of the following to your `index.js` file.

###### CSS
```js
import '@picocss/pico/css/pico.min.css'
```

###### SCSS
```js
import '@picocss/pico/scss/pico.slim.scss'
```

##### CDN
To Install PicoCSS using CDN add:
```html
<link rel="stylesheet" href="https://unpkg.com/@picocss/pico@latest/css/pico.min.css">
```
to the `<head>` of your website.

#### First Component
To get started we can simply import a component and get started immediately.

```js
import React from 'react'
import Card from 'react-picocss/Card'
import HGroup from "react-picocss/HGroup"

export default function ExampleCard(props) {
    return (
        <Card>
            <HGroup>
                <h1>Hellow World!</h1>
                <h2>Welcome to <code>react-picocss</code></h2>
            </HGroup>
        </Card>
    )
}
```

###### Adding Custom Classes
Simply pass style as you normally would the the `className` prop and your classes will be added to the element.
```js
<Card className="red"/>
```

Will render as:
```html
<div class='card red'></div>
```

## Change Log
A complete changelog of all changes, developmental and production, is included in [CHANGELOG.md](https://gitlab.com/aj-rom/react-picocss/-/blob/main//CHANGELOG.md).

### Code of Conduct
This package and all contributors should adhere to the [Code of Conduct](https://gitlab.com/aj-rom/react-picocss/-/blob/main//CODE_OF_CONDUCT.md).

## License
This package is available as open-sourced under the [MIT License](https://gitlab.com/aj-rom/react-picocss/-/blob/main//LICENSE).
