## [1.0.5](https://gitlab.com/aj-rom/react-picocss/compare/v1.0.4...v1.0.5) (2022-01-20)


### Bug Fixes

* **npm:** add repository url ([9203a5d](https://gitlab.com/aj-rom/react-picocss/commit/9203a5d71c64b4db2d487b3c185845043dcc9896))

## [1.0.4](https://gitlab.com/aj-rom/react-picocss/compare/v1.0.3...v1.0.4) (2022-01-20)


### Bug Fixes

* **npm:** fix homepage link ([eef8e84](https://gitlab.com/aj-rom/react-picocss/commit/eef8e84e15afde50b1c864bc0eaf9e64bc0373e3))

## [1.0.3](https://gitlab.com/aj-rom/react-picocss/compare/v1.0.2...v1.0.3) (2022-01-20)


### Bug Fixes

* **Release:** add changelog to git assets ([821b4bf](https://gitlab.com/aj-rom/react-picocss/commit/821b4bf0d0a970676bc8e13c43c4d12bcf3619f7))
