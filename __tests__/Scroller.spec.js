import React from 'react'
import { render } from 'react-dom'
import { act } from 'react-dom/test-utils'
import Scroller from '../dist/Scroller'

let container = null
beforeEach(() => {
    container = document.createElement('div')
    document.body.appendChild(container)
})

afterEach(() => {
    document.body.removeChild(container)
    container = null
})

test('is a figure type', () => {
    act( () => {
        render(<Scroller/>, container)
    })

    expect(container.firstChild.nodeName).toBe('FIGURE')
})

test('can handle the passing of css classes', () => {
    act( () => {
        render(<Scroller className='test'/>, container)
    })

    expect(container.firstChild.className).toBe('test')
})

test('can display children', () => {
    act( () => {
        render(<Scroller>test</Scroller>, container)
    })

    expect(container.firstChild.textContent).toBe('test')
})
