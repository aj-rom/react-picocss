import React from 'react'
import { render } from 'react-dom'
import { act } from 'react-dom/test-utils'
import HGroup from '../dist/HGroup'

let container = null
beforeEach(() => {
  container = document.createElement('div')
  document.body.appendChild(container)
})

afterEach(() => {
  document.body.removeChild(container)
  container = null
})

test('loads with css ".headings" class', () => {
  act(() => {
    render((
        <HGroup>
          <h1>Test</h1>
          <h2>Test</h2>
        </HGroup>
    ), container)
  })

  const element = container.firstChild
  expect(element.nodeName).toBe('DIV')
  expect(element.className).toBe('headings')
})

test('displays children', () => {
  act(() => {
    render(
      (
        <HGroup>
          <h1>Hello Test Suite</h1>
          <h2>Some SubHeading</h2>
        </HGroup>
      ), container,
    )
  })

  const element = container.firstChild
  expect(element.childNodes.length).toBe(2)
  expect(element.firstChild.nodeName).toBe('H1')
})
