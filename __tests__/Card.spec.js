import React from 'react'
import { render } from 'react-dom'
import { act } from 'react-dom/test-utils'
import Card from '../dist/Card.js'

let container = null
beforeEach(() => {
  container = document.createElement('div')
  document.body.appendChild(container)
})

afterEach(() => {
  document.body.removeChild(container)
  container = null
})

test('loads and displays with card class.', () => {
  act(() => {
    render(<Card />, container)
  })

  expect(container.firstChild.className).toBe('card')
})

test('allows passing of css classes', () => {
  act(() => {
    render(<Card className="red-box" />, container)
  })

  expect(container.firstChild.className).toBe('red-box card')
})
