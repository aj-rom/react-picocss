import React from 'react'
import { render } from 'react-dom'
import { act } from 'react-dom/test-utils'
import Link from '../dist/Link'

let container = null
beforeEach(() => {
  container = document.createElement('div')
  document.body.appendChild(container)
})

afterEach(() => {
  document.body.removeChild(container)
  container = null
})

test('loads and displays normal link.', () => {
  act(() => {
    render(<Link href="test" />, container)
  })
  const element = container.firstChild
  expect(element.nodeName).toBe('A')
  expect(element.getAttribute('href')).toBe('test')
})

test('loads and displays with secondary class.', () => {
  act(() => {
    render(<Link.Secondary href="test" />, container)
  })
  const element = container.firstChild
  expect(element.className).toBe('secondary')
})

test('loads and displays with contrast class.', () => {
  act(() => {
    render(<Link.Contrast href="test" />, container)
  })
  const element = container.firstChild
  expect(element.className).toBe('contrast')
})
