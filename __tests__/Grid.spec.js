import React from 'react'
import { render } from 'react-dom'
import { act } from 'react-dom/test-utils'
import Grid from '../dist/Grid.js'

let container = null
beforeEach(() => {
  container = document.createElement('div')
  document.body.appendChild(container)
})

afterEach(() => {
  document.body.removeChild(container)
  container = null
})

test('loads and displays with Grid class.', () => {
  act(() => {
    render(<Grid />, container)
  })
  expect(container.firstChild.className).toBe('grid')
})

test('allows passing of css classes', () => {
  act(() => {
    render(<Grid className="red-box" />, container)
  })

  expect(container.firstChild.className).toBe('red-box grid')
})
