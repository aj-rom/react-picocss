import React from 'react'
import { render } from 'react-dom'
import { act } from 'react-dom/test-utils'
import Container from '../dist/Container.js'

let container = null
beforeEach(() => {
  container = document.createElement('div')
  document.body.appendChild(container)
})

afterEach(() => {
  document.body.removeChild(container)
  container = null
})

test('loads and displays with container class', () => {
  act(() => {
    render(<Container />, container)
  })

  expect(container.firstChild.className).toBe('container')
})

test('can use props to set to a fluid container', () => {
  act(() => {
    render(<Container fluid />, container)
  })

  expect(container.firstChild.className).toBe('container-fluid')
})
