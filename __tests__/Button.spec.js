import React from 'react'
import { render } from 'react-dom'
import { act } from 'react-dom/test-utils'
import Button from '../dist/Button'

let container = null
beforeEach(() => {
    container = document.createElement('div')
    document.body.appendChild(container)
})

afterEach(() => {
    document.body.removeChild(container)
    container = null
})

test('renders a button', () => {
    act( () => {
        render(<Button/>, container)
    })

    expect(container.firstChild.nodeName).toBe('BUTTON')
})

test('can create an outlined button', () => {
    act( () => {
        render(<Button outline={ true }/>, container)
    })

    expect(container.firstChild.className).toBe('outline')
})

test('can render a secondary outlined button', () => {
    act( () => {
        render(<Button.Secondary outline={ true }/>, container)
    })

    expect(container.firstChild.className).toBe('secondary outline')
})

// test('', () => {
//     act( () => {
//
//     })
//
//     expect()
// })
//
// test('', () => {
//     act( () => {
//
//     })
//
//     expect()
// })
